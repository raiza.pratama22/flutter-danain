/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.hoc.flutter_danain;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.hoc.flutter_danain";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 8;
  public static final String VERSION_NAME = "3.3.0";
}
