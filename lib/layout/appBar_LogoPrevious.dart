import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_danain/pages/login/login.dart';
import 'package:flutter_svg/flutter_svg.dart';
PreferredSizeWidget logoPreviousWidget(BuildContext context){
  return AppBar(
    backgroundColor: Colors.white,
    elevation: 0.0,
    leading: IconButton(
        onPressed: () {
          Navigator.pushNamed(context, LoginPage.routeName );
        },
        icon: Icon(
          Icons.arrow_back,
          color: Colors.black,
        )),
    title: SvgPicture.asset(
      "assets/images/logo/Danain1.svg",
      width: 65,
      height: 24,
    ),
    centerTitle: true,
  );

}