import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

//importing file app_drawer.dar and appbar.dart
import 'package:flutter_danain/layout/appBar_LogoPrevious.dart';
import 'package:flutter_danain/layout/footer_Lisence.dart';

class OnboardingBorrowerPage extends StatelessWidget{
  static const routeName = '/home_borrower_page';
  const OnboardingBorrowerPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: logoPreviousWidget(context),
        //set drawer from app_drawer.dart
        //set like this where ever you want
        body: Center(
            child: Text("Reusable Drawer and Appbar",
              style: TextStyle(fontSize: 20),
            )
        ),
      bottomNavigationBar: footerLisence(context),

    );
  }
}